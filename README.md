### Introducción

Éste proyecto estudia e implementa la forma de conectarse a la api proporcionada por Leshan server demo para el protocolo LwM2M de OMA.

### Dependencias:
* Para los logs de la aplicación, **log4js**: [GitHub](https://github.com/log4js-node/log4js-node.git)
* Para la escucha de eventos, **eventsource**: [GitHub](https://github.com/EventSource/eventsource.git)
* Para las consultas get,put, post y delete, **request**: [GitHub](https://github.com/request/request.git)

### Instalación:
Desde el directório del prollecto con el código ya clonado/descargado y descomprimido, ejecutamos: 
> npm install

Esto instalará las dependencias necesaras para la ejecuvión.

### Configuración:
* Los archivos de configuración se almacenan en el directorio *env*.
* Su formato es JSON y como mínimo ha de tener el campo *logs*:

dev.json :

>        {
>            "config": {
>                "port": 3000
>            },
>            "auth": {
>                
>            },
>            "db": {
>                "host": "127.0.0.1",
>                "user": "root",
>                "password": "",
>                "secure": false 
>            },
>            "logs": {
>                "path": "./logs/appLog.log"
>            },
>            "serverLwm2m": {
>                "host": "127.0.0.1",
>                "port": 8080,
>                "secure": false 
>            }
>        }

* Por defecto se incluyen 2 ficheros de configuración de ejemplo: *dev.json* y *qa.json*.

### Estructura :
* ***SRC*** incluye el codigo .js principal:
    * *index.js*: hilo principal. Dependiente de *environment.js* y  *logging.js*.

    * *environment.js*: Independiente, puede añadirse a otro código instalando la dependencia *log4js*.
        + Cargando las propiedades del entorno desde un "archivo de propiedades" especificado como argumento en
            línea de comandos con env=`<nombreDelArchivo>`
        +   Si el SO es Windows, hay ciertas funciónes de linux que no están disponibles, éste código ha de
            solventarlar éste problema.
    
    * *logging.js*: configuración de los obetos de loging. Independiente, puene añadirse a otro código, teniendo en cuenta su forma de uso:
        > const [logsFile, consoleLogs, defaultLogs] = require('./logging')(`<pathAlmacenamientoFicheroLogs>`); 

### Ejecución:
* Es necesario el uso de ficheros de configuración **JSON** que han de almacenarse en el directorio ***env***.
* Desde el directorio en el que tenemos nuestro proyecto: 

> npm start env=`<nombreDelArchivo>`

### Comandos de acceso a los servtel de java:

# ObjectSpecServlet (/api/objectspecs)

* **GET** Para obtener de un equipo en concreto que objeto espera, o mas bien con qué objetos puede trabajar:
> http://localhost:8080/api/objectspecs/`endPoint`/

# ClientServlet (/api/clients)

* **GET** Para obtener todos los equipos registrados en el servidor lwm2m:
> http://localhost:8080/api/clients

Ejemplo:

    request.get('http://localhost:8080/api/clients/', (error, response, body) => {        
        consoleLogs.info(`Clientes: ${body}`);        
    }); 

* **GET** Para obtener todos los elementos de un equipo registrado, en ésta respuesta, dentro de objectLinks tendremos los links disponibles. Endpoint es el id del cliente lwm2m:
> http://localhost:8080/api/clients/`endPoint`

* **GET** Para acceder a los datos del equipo (atributos, recursos, objetos...) de la siguiente forma:
> http://localhost:8080/api/clients/`endPoint`/`objeto`/`instancia`      

* **GET** Para realizar una petición *discover* a un equipo sobre un recurso en concreto, obteniendo sus objectLinks para acceder a ellos:
> http://localhost:8080/api/clients/`endPoint`/`objeto`/discover
> http://localhost:8080/api/clients/`endPoint`/`objeto`/`instancia`/discover
> http://localhost:8080/api/clients/`endPoint`/`objeto`/`instancia`/`recurso`/discover

* **PUT** necesario al menos especificar, el equipo, id del objeto e instancia de éste, de ahí la granularidad de peticiones puede ser más fina, con la información nueva en el body del a petición:
> http://localhost:8080/api/clients/`endPoint`/`objeto`/`instancia`
> http://localhost:8080/api/clients/`endPoint`/`objeto`/`instancia`/`recurso`/`instancia`

    Ejemplo:

    const options = {     
        url: 'http://localhost:8080/api/clients/testlwm2mclient/31024/12/3',     
         method: 'PUT',       
         json: {          
            id: 3,        
            value: 32     
        }     
    }     
    request.put(options, (error, response, body) => {     
        console.log('error: ' + error);       
        console.log('response: ' + response);        
        console.log('body: ' + body.success);        
    });       

* **POST** Para mandar una petición de crear un recurso u objeto en el body a un cliente dado:
> http://localhost:8080/api/clients/`endPoint`/`objeto`/`instancia`
> http://localhost:8080/api/clients/`endPoint`/`objeto`/`instancia`/`recurso`/`instancia`

* **POST** Para iniciar una observación a un objeto/recurso:
> http://localhost:8080/api/clients/`endPoint`/`objeto`/`instancia`/observe
> http://localhost:8080/api/clients/`endPoint`/`objeto`/`instancia`/`recurso`/`instancia`/observe

*  **DELETE** Para cancelar una observación:
> http://localhost:8080/api/clients/`endPoint`/`objeto`/`instancia`/observe
> http://localhost:8080/api/clients/`endPoint`/`objeto`/`instancia`/`recurso`/`instancia`/observe

* **DELETE** Para eliminar una instancia: 
> http://localhost:8080/api/clients/`endPoint`/`objeto`/`instancia`
> http://localhost:8080/api/clients/`endPoint`/`objeto`/`instancia`/`recurso`/`instancia`

# EventServlet
Eventos disponibles desde el servtel de leshan:

    EVENT_DEREGISTRATION = "DEREGISTRATION";   
    EVENT_UPDATED = "UPDATED";     
    EVENT_REGISTRATION = "REGISTRATION";   
    EVENT_AWAKE = "AWAKE"; 
    EVENT_SLEEPING = "SLEEPING";   
    EVENT_NOTIFICATION = "NOTIFICATION";   
    EVENT_COAP_LOG = "COAPLOG";

* Escuchando en la ruta http://localhost:8080/event podemos estar atentos a los eventos anteriormente mencionados empleando el modulo EventSource como se indica en éste ejemplo:

Ejemplo:

    var eventSource = new EventSource('http://localhost:8080/event');
    eventSource.addEventListener("REGISTRATION", (event)=>{       
        console.log('Nuevo equipo registrado: '+event.data);        
    });       
    eventSource.addEventListener("DEREGISTRATION", (event)=>{     
        console.log('Nuevo equipo de ha desregistrado:'+event.data);      
    }); 

* Igualmente podemos escuchar los eventos específicos de un equipo end-point en concreto:

Ejemplo:

    var eventSource = new EventSource('http://localhost:8080/event?ep=`endPoint`');     
    eventSource.addEventListener("REGISTRATION", (event)=>{       
        console.log('Nuevo equipo registrado: '+event.data);        
    });       
    eventSource.addEventListener("DEREGISTRATION", (event)=>{     
        console.log('Nuevo equipo de ha desregistrado:'+event.data);      
    });       



# SecurityServlet (/api/security/`<path>`)
* **GET** Para obtener el certificado de seguridad del cliente:
> http://localhost:8080/api/security/clients

* **GET** Para obtener el certificado de seguridad del servidor:
> http://localhost:8080/api/security/server

* **PUT** Para actualizar la información de seguridad de los equipos end-points:
> http://localhost:8080/api/security/clients

* **DELETE** Para borrar el certificado del cliente:
> http://localhost:8080/api/security/clients

### Autor:
Miguel San Bernardino Martín