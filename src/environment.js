/*
    ====================================================================================================================
    Autor: Miguel San Bernardino Martín
    Fecha: 20 - nov - 2019
    Descripción: 
        +   Cargando las propiedades del entorno desde un "archivo de propiedades" especificado como argumento en
            línea de comandos con env=<nombreDelArchivo>
        +   Si el SO es Windows, hay ciertas funciónes de linux que no están disponibles, éste código ha de
            solventarlar éste problema.
    ====================================================================================================================
*/
'use strict';

/*===================================== Cargando las propiedades del entorno ===========================================*/

var count = 0 ; // count ==> gracias a la vriable count, sabremos cuando ha finalizado el bucle forEach (es asíncrono)
// Iniciamos un bucle forEach que recorra los elementos del array del proceso process.argv
process.argv.forEach((val, index, array) => {
    count++;
    var arg = val.split("="); // Separamos si aplica : env=dev -> [env,dev]
    if (arg.length > 0) { // Si hay elementos en el array...
        if (arg[0] === 'env') { // si se desae dar un environment file como argumento...
            try{
                var env = require('../env/' + arg[1] + '.json');
                if(env != undefined){
                    module.exports = env;
                }else{
                    console.error(`[\x1b[32m${new Date().toLocaleString()}\x1b[0m] 
                                    [\x1b[31mERROR\x1b[0m] No se ha especificado un 
                                    nombre correcto para el archivo de propiedades:\n 
                                    \x1b[31m${arg}\n`);
                    process.exit(1);
                }
            }catch(err){
                console.error(`[\x1b[32m${new Date().toLocaleString()}\x1b[0m] 
                                [\x1b[31mERROR\x1b[0m] 
                                No se encuentra el archivo de propiedades 
                                pasado por argumento:\n \x1b[31m${err}\n`);
                process.exit(1);
            }
        }
    }
    if (count === process.argv.length) { // Si se ha llegado al fin del bucle...
        if (typeof env === 'undefined'){ // Si no se ha transferido ningún archivo de propiedades
            console.error(`[\x1b[32m${new Date().toLocaleString()}\x1b[0m] 
                            [\x1b[33mWARN\x1b[0m] Es necesario pasar un archivo 
                            de propiedades como argumento: 
                            \x1b[7menv=<nombreDelArchivo>\x1b[0m\n \n`);
            process.exit(1);
        }
    }
});

/*=====================================================================================================================*/

/*============================================= Si el SO es Windows ===================================================*/

//  Si el sistema operativo es Windows Ctrl+c no desencadena la misma señal, hay que generarla...
/*  
    readLine ==> El módulo readline proporciona una interfaz para leer datos de una secuencia 
    legible (como process.stdin) línea por línea. 
*/
const readLine = require('readline'); 

/*
    process.platform ==> La propiedad process.platform devuelve una cadena que identifica la plataforma del sistema 
    operativo en la que se ejecuta el proceso Node.js:
    'aix': AXI OS (IBM)
    'darwin':  Mac OS X
    'freebsd': FreeBSD SO 
    'linux': GNU/Linux OS (OpenSource)
    'openbsd': OpenBSD
    'sunos': SunOS/Solaris (Sun Microsystems)
    'win32': Windows OS (Microsoft)
*/
if (process.platform === 'win32') {
    const readLineInterface = readLine.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    // Si se detecta un Ctrl+c en windows, se emite una señal SIGINT
    readLineInterface.on('SIGINT',() => {
        process.emit('SIGINT');
    });
}

/*=====================================================================================================================*/

/*
    REFERENCIA DE COLORES PARA LA CONSOLA:
        * Actuadores:
            Reset = "\x1b[0m"
            Bright = "\x1b[1m"
            Dim = "\x1b[2m"
            Underscore = "\x1b[4m"
            Blink = "\x1b[5m"
            Reverse = "\x1b[7m"
            Hidden = "\x1b[8m"

        * Color:
            FgBlack = "\x1b[30m"
            FgRed = "\x1b[31m"
            FgGreen = "\x1b[32m"
            FgYellow = "\x1b[33m"
            FgBlue = "\x1b[34m"
            FgMagenta = "\x1b[35m"
            FgCyan = "\x1b[36m"
            FgWhite = "\x1b[37m"
            
        * Background color:
            BgBlack = "\x1b[40m"
            BgRed = "\x1b[41m"
            BgGreen = "\x1b[42m"
            BgYellow = "\x1b[43m"
            BgBlue = "\x1b[44m"
            BgMagenta = "\x1b[45m"
            BgCyan = "\x1b[46m"
            BgWhite = "\x1b[47m"
*/