/*
    ====================================================================================================================
    Autor: Miguel San Bernardino Martín
    Fecha: 19 - nov - 2019
    Descripción: configuración de los obetos de loging.
    ====================================================================================================================
*/
'use strict';

/*================================================== LOGING ON =========================================================*/
const log4js = require('log4js'); // Carga el módulo log4s

module.exports = (path) =>{

    // Configuración del módulo log4js, niveles válidos: ALL, TRACE, DEBUG, INFO, WARN, ERROR, FATAL, MARK, OFF
    // De izquierda a derecha, si se escoge el nivel ERROR, se guardaran: ERROR, FATAL, MARK y OFF, y así con el resto de casos.
    log4js.configure({
        appenders: { 
            cheeseLogs: { type: 'file', filename: path }, // logs que se guardarán en el fichero
            console: { type: 'console' } // logs que se van a ver por consola
        },
        categories: { 
            cheeseLogs: { appenders: ["cheeseLogs"], level: 'INFO' },
            another: { appenders: ['console'], level: 'ALL' },
            default: { appenders: ['console', 'cheeseLogs'], level: 'ALL' }
        }
    });
    
    // Inicia un objeto del tipo log4js con la categoría deseada de las configuradas previamente
    const logsFile = log4js.getLogger('cheeseLogs');
    const consoleLogs =  log4js.getLogger('another');
    const defaultLogs = log4js.getLogger('default');
    
    return [logsFile, consoleLogs, defaultLogs];

    // Documentación: https://github.com/log4js-node/log4js-node
    // Ejemplo de uso:
    //      defaultLogs.info(`START ...`);

}

/*=====================================================================================================================*/