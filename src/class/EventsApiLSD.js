
'use strict';
/*
    ====================================================================================================================
    Autor: Miguel San Bernardino Martín
    Fecha: 21 - nov - 2019
    Descripción: Clase dedicada a la interacción con leshan server. Ésta clase depende de los módulos npm:
        * eventsource
        * request
        * log4js
        No depende de nungún codigo no perteneciénte a npm por lo que actua como caja negra.
    ====================================================================================================================
*/
const EventSource = require('eventsource');
const request = require('request');


module.exports = class EventsApiLSD {
    
    constructor( configApiAccess = { 
        host: '127.0.0.1', 
        port: 8080,
        secure: false 
    }){
        // Nombre de la clase
        this.className = 'EventsApiLSD';

        // Objeto con la configuración de acceso a la api de Leshan Server Demo
        this.configApiAccess = configApiAccess;

        if (configApiAccess.secure === true){
            this.httpType = 'https';
        } else {
            this.httpType = 'http';
        }

        // Posibles eventos a escuchar ...
        this.EVENT_DEREGISTRATION = 'DEREGISTRATION';   
        this.EVENT_UPDATED = 'UPDATED';     
        this.EVENT_REGISTRATION = 'REGISTRATION';   
        this.EVENT_AWAKE = 'AWAKE'; 
        this.EVENT_SLEEPING = 'SLEEPING';   
        this.EVENT_NOTIFICATION = 'NOTIFICATION';   
        this.EVENT_COAP_LOG = 'COAPLOG';
    }

    registrationListener(
        
        type = [
            this.EVENT_DEREGISTRATION,
            this.EVENT_UPDATED,
            this.EVENT_REGISTRATION,
            this.EVENT_AWAKE,
            this.EVENT_SLEEPING,
            this.EVENT_NOTIFICATION,
            this.EVENT_COAP_LOG
        ],
        callback = undefined
        
        ){
        const functionName = 'RegistrationListener';
        try {
            // Filtrado del array de eventos ...
            if(typeof type != 'object'){
                throw(`El recurso type pasado como argumento ha de ser del tipo 'object'.`);
            }
            var eventsArray = [];
            [this.EVENT_DEREGISTRATION, 
                this.EVENT_UPDATED, 
                this.EVENT_REGISTRATION, 
                this.EVENT_AWAKE,
                this.EVENT_SLEEPING,
                this.EVENT_NOTIFICATION,
                this.EVENT_COAP_LOG ].forEach((element) => {

                let typeEvent = type.find((e) => {
                    return e == element;
                });
                if(typeEvent !== undefined){
                    eventsArray.push(typeEvent);
                }

            });
            type = JSON.parse( JSON.stringify( eventsArray ) );
            if (type.length === 0){
                throw(`No se ha especificado ningún tipo de evento válido`);
            }

            // Generación de escucha de eventos ... 
            const newEvent = (eventName, event, callback) => {
                if (typeof callback == "function") 
                    callback(undefined, eventName, event);
            }

            // Conexión con EventServtel.java de Leshan Server Demo ...
            const eventSource = new EventSource(
                `${this.httpType}://${this.configApiAccess.host}:${this.configApiAccess.port}/event`
            );
            
            // Escuchando los posibles eventos ...
            type.forEach((element) => {
                switch(element){
                    case this.EVENT_DEREGISTRATION:{
                        eventSource.addEventListener(this.EVENT_DEREGISTRATION, (event)=>{
                            newEvent(this.EVENT_DEREGISTRATION, event, callback);
                        });
                        break;
                    }
                    case this.EVENT_UPDATED:{
                        eventSource.addEventListener(this.EVENT_UPDATED, (event)=>{
                            newEvent(this.EVENT_UPDATED, event, callback);
                        });
                        break;
                    }
                    case this.EVENT_REGISTRATION:{
                        eventSource.addEventListener(this.EVENT_REGISTRATION, (event)=>{
                            newEvent(this.EVENT_REGISTRATION, event, callback);
                        });
                        break;
                    }
                    case this.EVENT_AWAKE:{
                        eventSource.addEventListener(this.EVENT_AWAKE, (event)=>{
                            newEvent(this.EVENT_AWAKE, event, callback);
                        });
                        break;
                    }
                    case this.EVENT_SLEEPING:{
                        eventSource.addEventListener(this.EVENT_SLEEPING, (event)=>{
                            newEvent(this.EVENT_SLEEPING, event, callback);
                        });
                        break;
                    }
                    case this.EVENT_NOTIFICATION:{
                        eventSource.addEventListener(this.EVENT_NOTIFICATION, (event)=>{
                            newEvent(this.EVENT_NOTIFICATION, event, callback);
                        });
                        break;
                    }
                    case this.EVENT_COAP_LOG:{
                        eventSource.addEventListener(this.EVENT_COAP_LOG, (event)=>{
                            newEvent(this.EVENT_COAP_LOG, event, callback);
                        });
                        break;
                    }
                }
            });
            return true;
        } catch(err) {
            // si ha surgido algún error ...
            callback(`${this.className} => Función ${functionName}: \n ${err}`);
            return false;
        }
    }
}




/*
request.get(`http://${environment.serverLwm2m.host}:${environment.serverLwm2m.port}/api/clients`, (error, response, body) => {
    consoleLogs.info(`Clientes: ${body}`);
});

const options = {
     url: `http://${environment.serverLwm2m.host}:${environment.serverLwm2m.port}/api/clients/testlwm2mclient/31024/12/3`,
     method: 'PUT', 
     json: {
        id: 3,
        value: 32
    }
}

request.put(options, (error, response, body) => {
    consoleLogs.info(`error: ${error}`);
    consoleLogs.info(`response: ${response}`);
    consoleLogs.info(`body: ${body.success}`);
});
*/