/*
    ====================================================================================================================
    Autor: Miguel San Bernardino Martín
    Fecha: 21 - nov - 2019
    Descripción: hilo principal.
    ====================================================================================================================
*/
'use strict';

const environment = require('./environment'); // Cargamos la configuración de la aplicación
const [logsFile, consoleLogs, defaultLogs] = require('./logging')(environment.logs.path); // Cargamos los objetos de logging
defaultLogs.info('Inicio de proceso...');

/*=======================================================================================================================*/
const EventsApiLSD = require('./class/EventsApiLSD'); // Obtengo la clase EventsApiLSD
const mongodb = require('mongodb');
/*
    Genero un objeto del tipo EventsApiLSD para el mantenimiento de una base de datos actualizada y operaciónes
    contra la Api del servidor de lesahn en el puerto y dirección configurados en: environment.serverLwm2m
*/
const accessApi = new EventsApiLSD(environment.serverLwm2m);


var eventTypes = ['DEREGISTRATION', 'UPDATED', 'REGISTRATION', 'AWAKE', 'SLEEPING', 'NOTIFICATION', 'COAPLOG'];
accessApi.registrationListener(eventTypes, (err, type, event) => {
    
    if (!err) {
        var data = JSON.parse(event.data);
        defaultLogs.info(` => ${type} - ${data.endpoint}: \n  ${event.data}`);
    } else {
        defaultLogs.error(err);
    }

}) ? defaultLogs.info(`Escuchando eventos: ${eventTypes}`):defaultLogs.error(`Error iniciando escucha`);
/*=======================================================================================================================*/

// Detectando la señal Ctrl+c
process.on('SIGINT',() => {
    defaultLogs.warn('Interrupción detectada (Ctrl+C)... \n Finalizando ejecución ...');
    process.exit();
});
